
# Automated Development Environment

## Prerequisites:
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- Vagrant's VirtualBox Guest Additions Plugin (Install it by typing ``` vagrant plugin install vagrant-vbguest ``` into your terminal)

## To Spin Up a Development Environment
1. Clone this repo into your working directory ``` git clone https://bitbucket.org/kalebsmith/autodevenv.git ```
2. Run the Vagrantfile using ``` vagrant up ```
3. Kick back and watch Vagrant do its thing!

Vagrant pulls the base CentOS box down, installs *only the necessary packages*, and then runs an Ansible playbook to handle the rest of the configuration.
